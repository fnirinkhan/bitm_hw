// empty()
<?php
//Example1: a simple empty()/ issset comparison

$var =0; //evaluates to true  bcuz $var is empty
if (empty($var)) {
    echo '$var is either 0, empty,or not set at all';
     
}

//evalute as true bcus $var is set
if (isset($var)){
    echo '$var is set even though it is empty';
 }
 echo"<br>";
 ?>


<?php 
// example2: empty() on string offsets

$expectes_array_got_string = 'somestring';
var_dump(empty($expectes_array_got_string['its a string']));
var_dump(empty($expectes_array_got_string['34']));
var_dump(empty($expectes_array_got_string['0.5']));
var_dump(empty($expectes_array_got_string[4.5]));
var_dump(empty($expectes_array_got_string['5 mixd']));
var_dump(empty($expectes_array_got_string['0 mixd']));
var_dump(empty($expectes_array_got_string[0]));
echo "<br>";
?>
// is_array------------------------------------------------------
<?php 
// example3 check that variable is an array
echo "<br>";
$yes = array('this','is','an','array');
echo is_array($yes)?'Array':'not an array';
echo "<br>";

$no = 'this is a string';
echo is_array($no)? 'Array':'not an array';
echo '<br>';
?>
//is_bool------------------------------------------------------
<?php
//example4 is_bool()
echo '<br>';
$a =false;
$b =0;

if(is_bool($a)===true){
    echo 'yes';
} 
echo '<br>';
if (is_bool($b)===false){
    echo 'No';
}
echo '<br>';
?>
//is_int----------------------------------------------
<?php 
//example5 is_int()
echo '<br>';
$values = array("23",23,"23.3",23.3,null,true,false);
foreach ($values as $value){
    echo "is_int(";
    var_export($value);
    echo ') = ';
    var_dump(is_int($value));
    echo '<br>';
}
echo '<br>';
?>
//isset--------------------------------------------------
<?php 
echo '<br>';
//example6 isset()
$var ='';

if(isset($var)){
    echo 'this var is set so i will be printed';
}
echo '<br>';
$a= "test";
$b= "anothertest";

var_dump(isset($a));          //true

var_dump(isset($a,$b));       //true
echo '<br>';
unset($a);

var_dump(isset($a));
var_dump(isset($a,$b));
echo '<br>';
$foo = null;
var_dump(isset($foo));

echo '<br>';
?>
//print_r()---------------------------------------------
<?php
//example7 print_r()
echo '<br>';
$a = array('a' => 'apple','b' => 'banana','c' => array('X','Y','Z'));
 print_r($a);
        
   echo  '<br>';
?>
//serialize()---------------------------------------------
<?php 
echo '<br>';
//example8 serialize()
$conn = odbc__connect("webdb","php","html");
$stmt = odbc_prepare($conn,"UPDATE session SET data = ? WHERE id =?");
$sqldata = array(serialize($session_data), $_SERVER[PHP_AUTH_USER]);
if (!odbc_execute($stmt, $sqldata)){
    $stmt = odbc_prepare($conn, "INSERT INTO sessions (id,data)VALUES(?,?)");
    if(!odbc_execute($stmt, $sqldata)){

    }
}

echo '<br>';
?>
//unserialize()--------------------------------------------
<?php
echo '<br>';
//example9 unserialize()
$conn = odbc_connect("webdb","php","checken");
$stmt = odbc_prepare($conn,"SELECT data FROM sessions WHERE id = ?");
$sqldata = array($_SERVER['PHP_AUTH_USER']);
if (!odbc_execute($stmt,$sqldata) || !odbc_fetch_into($stmt,$tmp)){
    $session_data = array();
} else{
    $session_data = unserialize($tmp[0]);
    if(! is_array($session_data)){
        $session_data = array();
    }
}
echo '<br>';
?>

-----------------string handeling-----------------------------
//crypt()---------------------------------------------------
<?php 
echo "<br>";
//example10 crypt()
$password = 'mypassword';
$hash = crypt($password);
echo"<br>";
//
?>
//md5()------------------------------------------------------------
<?php 
echo "<br>";
////example11 md5()
$str = 'apple';
if(md5($str) === '1b28732c98473bnd838484'){
	echo "Would you like a green or red apple";
}

echo "<br>";
?>
//nl2br()-------------------------------------------------------
<?php 
echo"<br>";
//example12 nl2br();
echo nl2br("foo is not\n bar");

echo"<br>";
?>
//str_shuffle()-------------------------------------------------
<?php 
echo "<br>";
//example13 str_shuffle()
$str ='abcdef';
$shuffle + str_shuffle($str);
echo $shuffled;
echo"<br>";
?>
//atr_split()------------------------------------------------
<?php 
echo"<br>";
$str = "Hello world";
$str1 = str_split($str);
$str2 = str_split($str,3);
print_r($arr2);
print_r($arr2);

echo"<br>";

?>
//strlen()------------------------------------------------------
<?php 
echo "<br>";
$str = 'abcdef';
echo strlen($str);
$str = 'ab cd';
echo strlen($str);

echo"<br>";
?>
//strtolower()---------------------------------------------------
<?php 
echo"<br>";
$str = "hi it me irin khan";
$str = strtolower($str);
echo $str;
echo"<br>";
?>
//strtoupper()----------------------------------------------------
<?php 
echo"<br>";
$str = "hi irin khan";
$str = strtoupper($str);
echo $str;

echo"<br>";
?>






